//
//  ContactsViewController.swift
//  BaiNop-UITabbarViewController
//
//  Created by CPU11680 on 11/28/17.
//  Copyright © 2017 CPU11680. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var district:Array<String> = ["TP.HCM", "Hà Nội", "An Giang", "Đồng Tháp", "Kiên Giang", "Tiền Giang", "Cà Mau", "Sóc Trăng", "Tây Ninh", "Lâm Đồng", "Đồng Nai", "Đak Lak", "Ninh Thuận", "Bình Thuận"]
    
    var content:Array<String> = ["Miền Nam", "Miền Bắc", "Miền Nam", "Miền Nam", "Miền Nam", "Miền Nam", "Miền Nam", "Miền Nam", "Miền Nam", "Miền Trung", "Miền Nam", "Miền Trung", "Miền Trung", "Miền Trung"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return district.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblContacts.dequeueReusableCell(withIdentifier: "cellDistrict")
        cell?.textLabel?.text = district[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let src = storyboard?.instantiateViewController(withIdentifier: "DetailProvince") as! DetailProvinceController
        src.detail = content[indexPath.row]
        navigationController?.pushViewController(src, animated: true)
    }
    
    @IBOutlet weak var tblContacts: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblContacts.dataSource = self
        tblContacts.delegate = self
    }
}
