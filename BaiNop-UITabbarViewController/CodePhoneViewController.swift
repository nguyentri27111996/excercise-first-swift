//
//  CodePhoneViewController.swift
//  BaiNop-UITabbarViewController
//
//  Created by CPU11680 on 11/28/17.
//  Copyright © 2017 CPU11680. All rights reserved.
//

import UIKit

class CodePhoneViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var tabbar_MV: UITabBarItem!
    
    @IBOutlet weak var myLabel: UILabel!
    
    var district:Array<String> = ["TP.HCM", "Hà Nội", "An Giang", "Đồng Tháp", "Kiên Giang", "Tiền Giang", "Cà Mau", "Sóc Trăng", "Tây Ninh", "Lâm Đồng", "Đồng Nai", "Đak Lak", "Ninh Thuận", "Bình Thuận", "Lào Cai", "Thanh Hoá", "Huế", "Đà Nẵng", "Cần Thơ", "Vĩnh Long"]
    
    var content:Array<String> = ["28", "24", "296", "277", "297", "273", "298", "290", "231", "230", "256", "209", "211", "23", "209", "212", "213", "214", "216", "217"]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return district.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cltCode.dequeueReusableCell(withReuseIdentifier: "cellCode", for: indexPath) as! cellView
        cell.myLabel.text = district[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let src = storyboard?.instantiateViewController(withIdentifier: "DetailCodePhone") as! DetailCodePhoneController
        src.detail = "Mã vùng điện thoại: " + content[indexPath.row]
        navigationController?.pushViewController(src, animated: true)
    }
    
    @IBOutlet weak var cltCode: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cltCode.dataSource = self
        cltCode.delegate = self
    }

   
}
