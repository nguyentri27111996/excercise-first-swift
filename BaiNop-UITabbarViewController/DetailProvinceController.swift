//
//  DetailProvinceController.swift
//  BaiNop-UITabbarViewController
//
//  Created by CPU11680 on 11/29/17.
//  Copyright © 2017 CPU11680. All rights reserved.
//

import UIKit

class DetailProvinceController: UIViewController {

    @IBOutlet weak var txtDetailProvince: UITextView!
    
    var detail:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtDetailProvince.text = detail
    }

   

}
